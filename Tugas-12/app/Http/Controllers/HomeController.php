<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function welcome(){
       
        return view('welcome');
    }

    public function kirim(Request $request){
     $namaDepan = $request['firstname']; 
     $namaBelakang = $request['last-name']; 
     return view('welcome', ['namaDepan'=>$namaDepan,'namaBelakang'=>$namaBelakang]);
    
    }
}
