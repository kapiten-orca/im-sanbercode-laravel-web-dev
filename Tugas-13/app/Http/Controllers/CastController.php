<?php

namespace App\Http\Controllers;

use App\Models\cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jumlahbaris = 10;
        $data = cast::orderBy('id', 'desc')->paginate($jumlahbaris);
        return view('cast.index')-> with('data', $data);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = [
        'nama'=>$request->nama,
        'umur'=>$request->umur,
        'bio'=>$request->bio,
       ];
        cast::create($data);
        return redirect()->to('cast')->with('success','Berhasil menambahkan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = cast::where('id', $id)->first();
        return view('cast.edit')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required'=> 'Nama wajib diisi!',
            'umur.required'=> 'Jurusan wajib diisi!',
            'bio.required'=> 'Jurusan wajib diisi!',
    ]);
        $data = [
            'nama'=> $request->nama,
            'umur'=> $request->umur,
            'bio'=> $request->bio,
        ];
        cast::where('id', $id)->update($data);
        return redirect()->to('cast')->with('success','Berhasil melakukan update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        cast::where('id', $id)->delete();
        return redirect()->to('cast')->with('success', 'Berhasil melakukan delete data');
    }
}
