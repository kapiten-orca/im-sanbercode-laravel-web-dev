
<html>
    <body>
       
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action='{{url('kirim')}}' method="post">
            @csrf
            <label>First Name:</label><br>
            <input type="text" name="firstname" id="nama1"><br><br>
            <label>Last Name:</label><br>
            <input type="text" name="last-name" id="nama2" ><br><br>
            <label>Gender</label><br>
            <input type="radio" name="gender">Male <br>
            <input type="radio" name="gender">Female <br>
            <input type="radio" name="gender">Other <br><br>
            <label>Nationality</label><br>
            <select name="nationality" id="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Filipina">Filipina</option>
                <option value="Thailand">Thailand</option>
                <option value="Singapura">Singapura</option>
            </select><br><br>
            <label>Languange Spoken:</label><br>
            <input type="checkbox" name="Bahasa">Bahasa Indonesia <br>
            <input type="checkbox" name="English">English <br>
            <input type="checkbox" name="Other">Other <br><br>
            <label>Bio</label><br>
            <textarea name="buo" id="bio" cols="30" rows="10"></textarea><br><br>

            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>